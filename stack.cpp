﻿// stack.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#include<cassert>
#include <iostream>
#include<iomanip>
using namespace std;

class Stack
{
    int size;                
    int* array;
    int top;
public:
    Stack();
    Stack(int s);
    ~Stack();            //деструктор
    void Push(int a);
    int Pop();
    void Clear();
    bool isEmpty();
    bool isFull();
    void Print();

};
void main()
{
    Stack A(6);
    A.Push(3);
    A.Push(5);
    A.Push(16);
    A.Push(55);
    A.Push(30);
    A.Push(1);
    A.Push(7);
    A.Print();
    cout << "border" << "\n";
    A.Pop();
    A.Print();
    cout << "border" << "\n";
    A.Pop();
    A.Print();
    A.Clear();

}
Stack::Stack()
{
    size = 0;
    array = NULL;
    top = 0;
}
Stack::Stack(int s)
{
    size = s;
    array = new int[size];
    top = 0;
}
Stack::~Stack()
{
    delete[] array;
}
void Stack::Push(int a)
{
    if (top == size)
    {
        cout << "stack is full" << "\n" << endl;
    }
    else
    {
        array[top] = a;
        top++;
    }

}
int Stack::Pop()
{
    top--;
    return array[top];
}
void Stack::Clear()
{
    top = 0;
}
bool Stack::isEmpty()
{
    if (top == -1)
    {
        cout << "not empty" << "\n" << endl;
        return false;
    }
    else
    {
        cout << "Empty" << "\n" << endl;
        return true;
    }
}
bool Stack::isFull()
{
    if (top == size)
    {
        cout << "Full" << "\n" << endl;
        return true;
    }
    else
    {
        cout << "Empty" << "\n" << endl;
        return false;
    }
}
void Stack::Print()
{
    for (int i = top - 1; i >= 0; i--)
        cout << "|" << setw(4) << array[i] << endl;
}
